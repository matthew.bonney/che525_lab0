#%%
# Import modules =======================================================
import psi4
import numpy as np  
import matplotlib.pyplot as plt
import pandas as pd


#%%
# Set up Psi4 ==========================================================

psi4.core.clean()         
psi4.core.clean_options()
psi4.set_memory('8000 MB')
psi4.set_num_threads(4)    
psi4.core.set_output_file('Lab1resultsD1.dat', False)


#%%
# Define methods to use and .xyz files to read =========================
methods = ['scf/3-21G', 'scf/6-311G*'] # List of methods to use
mol_names = ['water' , 'water_170' , 'methanol_staggered' , 'methanol_eclipsed']

#%%
# Initialize Python dictionaries to store results ======================
    
molecules = {} # store molecule objects
E0 = {}        # store un-optimized energies
Eopt = {}      # store optimized energies
wfn_opt = {}   # store optimized wave functions

#%%
# Load molecules from .xyz files ========================================

f = open('water.xyz') #open .xyz file
molecules['water'] = psi4.geometry(f.read())
molecules['water'] = reset_point_group('c1') #turn symmetry off
E0['water'] = {}  #initialize an empty dictionary to store the E0 results
Eopt['water'] = 0 #initialize optimized energy to scalar 0

#%%
# Run calculations. Illustrate using a for loop =======================+=

for mol_name in mol_names:
    print('Working on ' + mol_name)    
    for meth_j in methods:                 #loop over methods
        E0[mol_name][meth_j] = psi4.energy(meth_j, molecule = molecules[mol_name])
    
    #use if statements to control the special cases of water_170 and methanol_eclipsed
    if mol_name == 'water_170':
        psi4.set_module_options('optking',{'frozen_bend': '1 2 3'})
        
    Eopt[mol_name], wfn_opt[mol_name] = psi4.optimize(methods[-1], molecule = molecules[mol_name], return_wfn = True)
    psi4.driver.molden(wfn_opt[mol_name], mol_name + '.molden')
    
     if mol_name == 'methanol_eclipsed':
        psi4.set_module_options('optking',{'frozen_bend': '1 2 3'})
        
    Eopt[mol_name], wfn_opt[mol_name] = psi4.optimize(methods[-1], molecule = molecules[mol_name], return_wfn = True)
    psi4.driver.molden(wfn_opt[mol_name], mol_name + '.molden')
    
#%%
# Print energy results to screeen ======================================

print('Energies at initial positions with different methods:')
print()
print(E0)
print()
print(methods[-1] + ' energies after optimization:')
print(Eopt)

#%%
# Save optimized results to a spreadsheet using Pandas
df = pd.DataFrame(data = Eopt, index = [0])
df.to_csv('Lab1resultsD1_Eopt.csv')  # save dataframe to .csv spreadsheet file.


#%%
# Save initial results to a spreadsheet using Pandas
df = pd.DataFrame(E0))
df.to_csv('Lab1results_D1_E0.csv')  # save dataframe to .csv spreadsheet file.

   